clementine (1.3.1+git609-g623a53681+dfsg-1) unstable; urgency=medium

  * New upstream snapshot. (Closes: #885567, #898998)
  * Bump Standards-Version to 4.2.1.
  * Cherry-pick an upstream patch to fix appstream data installation target.

 -- Thomas Pierson <contact@thomaspierson.fr>  Sat, 03 Nov 2018 12:58:42 +0100

clementine (1.3.1+git565-gd20c2244a+dfsg-1) unstable; urgency=medium

  * New upstream snapshot.
  * Remove obsolete depend on libqt4-sql-sqlite in debian/control file.
    (Closes: #901146)
  * Replace build-depend on liblastfm-dev (Qt4 build) by liblastfm5-dev (Qt5
    build) to reenable LastFm support. (Closes: #902988)
  * Update debian/copyright file.
  * Add a patch to fix the taglib version checking in CMakeLists.txt.

 -- Thomas Pierson <contact@thomaspierson.fr>  Wed, 15 Aug 2018 13:54:13 +0200

clementine (1.3.1+git542-gf00d9727c+dfsg-1) unstable; urgency=medium

  * New upstream snapshot.
  * List only current buildable architectures in debian/control file.
  * Update debian/copyright file.

 -- Thomas Pierson <contact@thomaspierson.fr>  Thu, 17 May 2018 20:31:52 +0200

clementine (1.3.1+git426-gb8381321c+dfsg-1) experimental; urgency=medium

  * New upstream snapshot of Qt5 branch.
  * Remove obsolete Qt4 build-depends and add new required Qt5 build-depends.
  * Refresh patches always_use_system_libmygpo-qt, freebsd_isnt_kfreebsd and
    hide_boost_includes_from_q_moc.
  * Remove libqxt related build options in debian/rules because we don't use
    the system libqxt any more and update debian/README.Debian accordingly.
  * Update debian/copyright file.
  * Bump Standards-Version to 4.1.4.
  * Move package repository to salsa.debian.org.
  * Set libmygpo-qt build-dependency to version 1.1.0-1 or above in
    debian/control.
  * Remove obsolete libqjson-dev build-dependency in debian/control.
  * Refresh and adapt always_use_system_libmygpo-qt.patch.

 -- Thomas Pierson <contact@thomaspierson.fr>  Wed, 09 May 2018 17:07:01 +0200

clementine (1.3.1+git276-g3485bbe43+dfsg-1) unstable; urgency=medium

  * New upstream snapshot. (Closes: #851005)
  * Update debian/copyright file.
  * Update package description in debian/control file.
  * Update the man page.
  * Refresh patches.

 -- Thomas Pierson <contact@thomaspierson.fr>  Sun, 15 Jan 2017 11:14:45 +0100

clementine (1.3.1+git240-g1a2f6e2+dfsg-1) unstable; urgency=medium

  * New upstream snapshot.
  * Remove redundant debian/gbp.cfg config file.
  * Update debian/gbp.conf file to new conventions.
  * Remove libechonest-dev from build dependencies as no longer needed.
  * Update debian/copyright file.
  * Add gstreamer1.0-plugins-bad as suggest in debian/control.
    (Closes: #834439)
  * Refresh all patches.

 -- Thomas Pierson <contact@thomaspierson.fr>  Tue, 18 Oct 2016 20:41:06 +0200

clementine (1.3.1+dfsg-1) unstable; urgency=low

  * New upstream release. (Closes: #833620, #827882)
  * Use DEB_CXXFLAGS_MAINT_APPEND instead of -DCMAKE_CXX_FLAGS in debian/rules
    to prevent override of other flags.
  * Remove obsolete debian/menu file.
  * Bump Standards-Version to 3.9.8.

 -- Thomas Pierson <contact@thomaspierson.fr>  Sat, 20 Aug 2016 22:42:52 +0200

clementine (1.3~rc1-1) unstable; urgency=low

  * New upstream release. (Closes: #736457, #784646, #810498)
  * Refresh patches
  * Use libmygpo-qt from Debian instead of the embedded code copy.
  * Update debian/copyright.

 -- Jérémy Bobbio <lunar@debian.org>  Wed, 27 Jan 2016 15:14:24 +0100

clementine (1.2.3+git1354-gdaddbde+dfsg-1) unstable; urgency=medium

  * New upstream snapshot.
  * Update dependencies for the new upstream snapshot requirments:
    - Remove build dependencies on libqca2-dev and libindicate-qt-dev.
    - Set build dependencies on libgstreamer1.0-dev and
    libgstreamer-plugins-base1.0-dev. (Closes: #785829)
    - Add build dependency on libcrypto++-dev and libpulse-dev.
    - Update dependencies to gstreamer1.0 plugins.
  * Remove obsolete patches.
  * Refresh patch: remove-references-to-non-dfsg-files.patch.
  * Refresh patch: hide_boost_includes_from_q_moc.patch.
  * Update patch fix_gcc5_ftbfs.patch because some changes are already
    included upstream.
  * Improve dh_clean rules to remove new generated files during the build.
  * Update README.Debian file about embedded copy of code changes and DFSG
    version.
  * Remove obsolete build options in debian/rules file.
  * Update and improve the debian/copyright file.
  * Update package repository location.

 -- Thomas Pierson <contact@thomaspierson.fr>  Sat, 03 Oct 2015 12:11:44 +0200

clementine (1.2.3+dfsg-4) unstable; urgency=medium

  * Improve dh_clean rules to remove all generated files during the build.
    Thanks to Timothy Potter for the patch. (Closes: #790541)
  * Add a patch to fix FTBFS with Boost 1.58. Thanks to Robert Bruce Park for
    the patch. (Closes: #795144, #794673)

 -- Thomas Pierson <contact@thomaspierson.fr>  Thu, 13 Aug 2015 20:05:56 +0200

clementine (1.2.3+dfsg-3) unstable; urgency=low

  * Add a patch to fix GCC-5 FTBFS. Thanks to Timothy Potter for his help on
    this issue. (Closes: #777819)
  * Remove Lisandro Damián Nicanor Pérez Meyer from Uploaders at his request.
    (Closes: #780753)
  * Remove Artur Rona from Uploaders because he is no longer involved in
    maintaining this package for a long time.
  * Bump Standards-Version to 3.9.6.

 -- Thomas Pierson <contact@thomaspierson.fr>  Wed, 15 Jul 2015 23:47:19 +0200

clementine (1.2.3+dfsg-2) unstable; urgency=medium

  * Change my email address to the Debian one.
  * Backport no_namespaces_for_dbus_interfaces.patch (Closes: #758448).

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 23 Aug 2014 20:22:01 -0300

clementine (1.2.3+dfsg-1) unstable; urgency=low

  * New upstream release. (Closes: #742163, #724615, #722471)
  * Update debian/watch file.
  * Bump Standards-Version to 3.9.5.
  * Bump debian/copyright standard version to 1.0.
  * Fix a misspelling issue in debian/copyright.

 -- Thomas Pierson <contact@thomaspierson.fr>  Sat, 12 Jul 2014 22:07:28 +0200

clementine (1.2.0+dfsg-2) unstable; urgency=low

  * Fix FTBFS on kfreebsd system using a patch from Modestas Vainius.
    (Closes: #729239)

 -- Thomas Pierson <contact@thomaspierson.fr>  Mon, 18 Nov 2013 16:58:19 +0100

clementine (1.2.0+dfsg-1) unstable; urgency=low

  * New upstream release. (Closes: #726474)
  * Update debian/copyright and debian/README.Debian files.
  * Add build-depend on libqca2 because it is now a hard build-depend.
  * Add cmake option -DI_HATE_MY_USERS=ON in debian/rules to allow build of
    clementine using the system sqlite library. See README.Debian file for more
    information about that.
  * Refresh patch: remove-references-to-non-dfsg-files.patch.
  * Remove obsolete patches:
    - fix-deprecated-function-with-glib236.patch
    - fix-islaptop-check-on-kfreebsd.patch
  * Remove useless build-depends on libimobiledevice-dev, libusbmuxd-dev,
    libplist-dev because upstream drops support for iMobileDevice.
    (Closes: #726339, #728434)

 -- Thomas Pierson <contact@thomaspierson.fr>  Sun, 27 Oct 2013 21:44:36 +0100

clementine (1.1.1+dfsg-2) unstable; urgency=low

  * Upload to unstable. (Closes: #701260)
  * Merge changes from 1.0.1+dfsg-3 revision.
  * Set build-depend liblastfm-dev minimum version to version 1.0.3 in
    coordination with liblastfm1 transition in unstable.
  * Remove minimum version 2.0.1 requirement on libechonest-dev build-depend
    because this version is not already available in unstable.
  * Add a patch fixing kFreeBSD build issue:
    - debian/patches/fix-islaptop-check-on-kfreebsd.patch

 -- Thomas Pierson <contact@thomaspierson.fr>  Sat, 01 Jun 2013 12:21:00 +0200

clementine (1.0.1+dfsg-3) unstable; urgency=low

  * Fix FTBFS caused by a deprecated function in glib2.36 using
    an upstream patch. (Closes: #707542)
  * Remove deprecated DM-Upload-Allowed field in debian/control file.
  * Bump Standards-Version to 3.9.4.

 -- Thomas Pierson <contact@thomaspierson.fr>  Mon, 27 May 2013 20:31:24 +0200

clementine (1.1.1+dfsg-1) experimental; urgency=low

  * New upstream release.
  * Add libfftw3-dev to build-depends for moodbar support (Closes: #694250)
  * Add libsparsehash-dev to build-depends and set minimum requirement to
    version 1.8 for libtag1-dev for Google Drive support. (Closes: #694924)
  * Update the debian/watch file. Thanks to Bart Martens.
  * Remove deprecated DM-Upload-Allowed field in debian/control file.
  * Set build-depend libechonest-dev minimum version to version 2.0.1.

 -- Thomas Pierson <contact@thomaspierson.fr>  Mon, 10 Dec 2012 23:03:05 +0100

clementine (1.1.0~rc1+dfsg-1) experimental; urgency=low

  * New upstream release candidate. (Closes: #639975)
  * Remove obsolete patches.
  * Add libprotobuf and protobuf-compiler to build-depends because it
    is now a hard requirement.
  * Update debian/copyright and debian/README.Debian files.
  * Add a lintian override binary-without-manpage because a new binary
    clementine-tagreader is only used internaly by Clementine and does not
    need a manpage.

 -- Thomas Pierson <contact@thomaspierson.fr>  Sun, 30 Sep 2012 20:27:06 +0200

clementine (1.0.1+dfsg-2) unstable; urgency=low

  [ Artur Rona ]
  * Merge changes from Ubuntu. Following patches have been applied
    by upstream and can be dropped with next upstream release.
  * debian/patches/fix-desktop-file.patch:
    - Clementine is not listed in media applications (right click
      > properties > open with) due to missing %U for Exec in
      desktop file. (Closes: #666966)
  * Upgrade Build-Depends on universal libglew-dev.
  * Bump Standards-Version to 3.9.3.
  * Upgrade to debhelper 9.

  [ Thomas Pierson ]
  * Fix clementine crash using version 4.8 of libqt4-sql
    (Closes: #660714, #673556).
    Thanks to David Sansome for fixing this upstream and thanks to Micah 
    Gersten for reporting this issue on debian bt.
    - add debian/patches/fix_qtsql.patch
    - update debian/patches/series
  * Set -DCMAKE_CXX_FLAGS="-DQT_NO_DEBUG_OUTPUT -DQT_NO_WARNING_OUTPUT" to
    stop debug output (Closes: #658224).
  * Update package repository URLs.
  * Add DM-Upload-Allowed field in debian/control.

 -- Thomas Pierson <contact@thomaspierson.fr>  Thu, 24 May 2012 19:54:29 +0100

clementine (1.0.1+dfsg-1) unstable; urgency=low

  * New upstream release. (Closes: #653926, #651611, #657391)

 -- Thomas PIERSON <web.pierson@gmail.com>  Sun, 01 Jan 2012 20:43:39 +0100

clementine (0.7.1+dfsg-4) unstable; urgency=low

  [ Artur Rona ]
  * Merge changes from Ubuntu:
    - Use wrap-and-sort script
    - Bump to debhelper 8
    - Specify Build-Depends
    - Remove blank line from d/install and d/watch
    - Add override for Changelog file
  * Fix d/copyright to make lintian happy:
    - Add missing licenses LGPL-2.1+ and GPL-3+
    - Specify Format header and add Upstream-Name

  [ Thomas Pierson ]
  * Add Lisandro and Artur to d/control as Co-Maintainers.
  * Add libindicate-qt-dev in Build-Depends to enable sound menu integration.

 -- Thomas PIERSON <web.pierson@gmail.com>  Mon, 10 Oct 2011 12:40:12 +0200

clementine (0.7.1+dfsg-3) unstable; urgency=low

  * Acknowledge NMU: 0.7.1+dfsg-2.1 . Thanks to Lisandro for his work.
  * Restore lost commits due to the NMU.

 -- Thomas PIERSON <web.pierson@gmail.com>  Thu, 18 Aug 2011 19:35:30 +0200

clementine (0.7.1+dfsg-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS by qFuzzyCompare function on armel, armhf and sh4. Thanks
    Nobuhiro Iwamatsu for the patch. This patch has already been applied
    upstream in current trunk (Closes: #636001).

 -- Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>  Wed, 03 Aug 2011 14:14:46 -0300

clementine (0.7.1+dfsg-2) unstable; urgency=low

  [ Thomas Pierson ]
  * Remove 'gstreamer0.10-alsa' from Depends and put 'gstreamer0.10-alsa |
    gstreamer0.10-pulseaudio' in Recommends instead. Closes: #630547
    Thanks Christian Marillat for reporting this bug.
  * Update Vcs-git URL.
  * Add information about the 'dfsg_clean' branch of the package
    repository in README.source file.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Remove unneeded explicit dependencies:
    - libechonest1.1.
    - libqxt-gui0.
    - libqxt-core0.

 -- Thomas PIERSON <web.pierson@gmail.com>  Thu, 07 Jul 2011 22:35:30 +0200

clementine (0.7.1+dfsg-1) unstable; urgency=low

  * Initial release. Thanks to Jérémy Bobbio for his sponsoring work.
    Thanks to Lisandro Pérez Meyer for his packaging work on libechonest
    depend. Thanks to Paul Grandperrin for his advices, patches and reviews
    about embedded libraries issues. Thanks to David Sansome for his
    upstream patches. Thanks to Arthur Rona for his advices.  Closes: #579859

 -- Thomas PIERSON <web.pierson@gmail.com>  Tue, 24 May 2011 01:25:23 +0200

